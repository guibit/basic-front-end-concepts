import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEstComponent } from './show-est.component';

describe('ShowEstComponent', () => {
  let component: ShowEstComponent;
  let fixture: ComponentFixture<ShowEstComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowEstComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
