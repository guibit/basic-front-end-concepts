import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';


@Component({
  selector: 'app-show-est',
  templateUrl: './show-est.component.html',
  styleUrls: ['./show-est.component.css']
})
export class ShowEstComponent implements OnInit {
  value = '';
  constructor(private service:SharedService) { }


  EstudanteList:any=[];

  ModalTitle:string;
  ActivateAddEditEstComp:boolean=false;
  dep:any;

  EstudanteIdFilter:string="";
  EstudanteNameFilter:string="";
  EstudanteListWithoutFilter:any=[];

  ngOnInit(): void {
    this.refreshDepList();
  }

  addClick(){
    this.dep={
      EstudanteId:0,
      EstudanteName:"",
      EstudanteEmail:"",
      EstudanteStatus:"",
      EstudanteTelefone:"",
      EstudanteDate:"",
    }
    this.ModalTitle="Adicionar Estudante";
    this.ActivateAddEditEstComp=true;

  }

  editClick(item){
    this.dep=item;
    this.ModalTitle="Editar Estudante";
    this.ActivateAddEditEstComp=true;
  }

  deleteClick(item){
    if(confirm('Você tem Certeza??')){
      this.service.deleteEstudante(item.EstudanteId).subscribe(data=>{
        alert(data.toString());
        this.refreshDepList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditEstComp=false;
    this.refreshDepList();
  }


  refreshDepList(){
    this.service.getEstudanteList().subscribe(data=>{
      this.EstudanteList=data;
      this.EstudanteListWithoutFilter=data;
    });
  }

  FilterFn(){
    var EstudanteIdFilter = this.EstudanteIdFilter;
    var EstudanteNameFilter = this.EstudanteNameFilter;

    this.EstudanteList = this.EstudanteListWithoutFilter.filter(function (el){
        return el.DepartmentId.toString().toLowerCase().includes(
          EstudanteIdFilter.toString().trim().toLowerCase()
        )&&
        el.DepartmentName.toString().toLowerCase().includes(
          EstudanteNameFilter.toString().trim().toLowerCase()
        )
    });
  }

  sortResult(prop,asc){
    this.EstudanteList = this.EstudanteListWithoutFilter.sort(function(a,b){
      if(asc){
          return (a[prop]>b[prop])?1 : ((a[prop]<b[prop]) ?-1 :0);
      }else{
        return (b[prop]>a[prop])?1 : ((b[prop]<a[prop]) ?-1 :0);
      }
    })
  }

}
