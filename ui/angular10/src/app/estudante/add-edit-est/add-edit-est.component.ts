import { Component, Input, OnInit,VERSION, ViewEncapsulation  } from '@angular/core';
import {SharedService} from 'src/app/shared.service';
import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { ChangeDetectionStrategy} from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';


export interface Fruit {
  name: string;
}
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-add-edit-est',
  templateUrl: './add-edit-est.component.html',
  styleUrls: ['./add-edit-est.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})

export class AddEditEstComponent implements OnInit {

  //tag
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  fruits: Fruit[] = [
  ];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add Tag
    if (value) {
      this.fruits.push({name: value});
    }

    // Clear the input value
    //event.chipInput!.clear();
  }

  remove(fruit: Fruit): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }
 
  //nome
  value = '';
  //fone
  name = 'Angular ' + VERSION.major;
  myForm: FormGroup;
  phoneNumber;
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  matcher = new MyErrorStateMatcher();
  

  constructor(private service:SharedService, private fb: FormBuilder) {
    this.myForm = this.fb.group({
      phone: [undefined, [Validators.required]],
    });
    
  }



  @Input() dep:any;
  EstudanteId:string;
  EstudanteName:string;
  EstudanteEmail:string;
  EstudantePhone:string;
  EstudanteTag:string;
  EstudanteDate:string;
  EstudanteStatus:boolean = true;


  ngOnInit(): void {
    this.EstudanteId=this.dep.EstudanteId;
    this.EstudanteName=this.dep.EstudanteName;
    this.EstudanteEmail=this.dep.EstudanteEmail;
    this.EstudantePhone=this.dep.EstudantePhone;
    this.EstudanteTag=this.dep.EstudanteTag;
    this.EstudanteStatus=this.dep.EstudanteStatus;
  }

  addEstudante(){
    var val = {EstudanteId:this.EstudanteId,
                EstudanteName:this.EstudanteName,
                EstudanteEmail:this.EstudanteEmail,
                EstudantePhone:this.EstudantePhone,
                EstudanteTag:this.EstudanteTag,
                EstudanteStatus:this.EstudanteStatus};
    this.service.addEstudante(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateEstudante(){
    var val = {EstudanteId:this.EstudanteId,
      EstudanteName:this.EstudanteName,
      EstudanteEmail:this.EstudanteEmail,
      EstudantePhone:this.EstudantePhone,
      EstudanteTag:this.EstudanteTag,
      EstudanteStatus:this.EstudanteStatus};
    this.service.updateEstudante(val).subscribe(res=>{
    alert(res.toString());
    });
  }


  
  submitPhone(){
    if (this.myForm.valid) {
           this.phoneNumber = this.myForm.get('phone').value;
      }
 }

 get phoneValue(){
   return this.myForm.controls['phone']
 }

}
