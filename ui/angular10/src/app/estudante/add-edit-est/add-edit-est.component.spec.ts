import { async,ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditEstComponent } from './add-edit-est.component';

describe('AddEditEstComponent', () => {
  let component: AddEditEstComponent;
  let fixture: ComponentFixture<AddEditEstComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditEstComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditEstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
