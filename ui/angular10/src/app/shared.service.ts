import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
readonly APIUrl = "http://127.0.0.1:8000";

  constructor(private http:HttpClient) { }

  //estudantes

  getEstudanteList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/students');
  }
  addEstudante(val:any){
    return this.http.post(this.APIUrl + '/students',val);
  }
  updateEstudante(val:any){
    return this.http.put(this.APIUrl + '/students',val);
  }
  deleteEstudante(val:any){
    return this.http.delete(this.APIUrl + '/students'+val);
  }

  getAllEstudanteList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/students');
  }

  //cursos

  getCursoList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/courses');
  }
  addCurso(val:any){
    return this.http.post(this.APIUrl + '/courses',val);
  }
  updateCurso(val:any){
    return this.http.put(this.APIUrl + '/courses',val);
  }
  deleteCurso(val:any){
    return this.http.delete(this.APIUrl + '/courses'+val);
  }

  //Matriculas

  getMatriculaList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/enrollments');
  }
  addMatricula(val:any){
    return this.http.post(this.APIUrl + '/enrollments',val);
  }
  updateMatricula(val:any){
    return this.http.put(this.APIUrl + '/enrollments',val);
  }
  deleteMAtricula(val:any){
    return this.http.delete(this.APIUrl + '/enrollments'+val);
  }

  //Catgoria de Cursos

  getCatCursoList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/course-categories/');
  }
  addCatCurso(val:any){
    return this.http.post(this.APIUrl + '/course-categories/',val);
  }
  updateCatCurso(val:any){
    return this.http.put(this.APIUrl + '/course-categories/',val);
  }
  deleteCatCurso(val:any){
    return this.http.delete(this.APIUrl + '/course-categories/'+val);
  }

}
