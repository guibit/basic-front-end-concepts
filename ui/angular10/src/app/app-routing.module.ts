import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {CursoComponent} from './curso/curso.component';
import {CursoCategoriaComponent} from './curso-categoria/curso-categoria.component';
import {EstudanteComponent} from './estudante/estudante.component';
import {MatriculaComponent} from './matricula/matricula.component';


const routes: Routes = [
  {path:'courses',component:CursoComponent},
  {path:'course-categories',component:CursoCategoriaComponent},
  {path:'students',component:EstudanteComponent},
  {path:'enrollments',component:MatriculaComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
