import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EstudanteComponent } from './estudante/estudante.component';
import { AddEditEstComponent } from './estudante/add-edit-est/add-edit-est.component';
import { ShowEstComponent } from './estudante/show-est/show-est.component';
import { CursoComponent } from './curso/curso.component';
import { ShowCursComponent } from './curso/show-curs/show-curs.component';
import { AddEditCursComponent } from './curso/add-edit-curs/add-edit-curs.component';
import { MatriculaComponent } from './matricula/matricula.component';
import { AddEditMatComponent } from './matricula/add-edit-mat/add-edit-mat.component';
import { ShowMatComponent } from './matricula/show-mat/show-mat.component';
import {SharedService} from './shared.service'; 

import {HttpClientModule} from '@angular/common/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { CursoCategoriaComponent } from './curso-categoria/curso-categoria.component';
import { ShowComponent } from './curso-categoria/show/show.component';
import { AddEditComponent } from './curso-categoria/add-edit/add-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import { NgxMatIntlTelInputModule  } from "ngx-mat-intl-tel-input";
import {MatButtonModule} from '@angular/material/button';
import {MatChipsModule} from '@angular/material/chips';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatOptionModule } from '@angular/material/core';





@NgModule({
  declarations: [
    AppComponent,
    EstudanteComponent,
    AddEditEstComponent,
    ShowEstComponent,
    CursoComponent,
    ShowCursComponent,
    AddEditCursComponent,
    MatriculaComponent,
    AddEditMatComponent,
    ShowMatComponent,
    CursoCategoriaComponent,
    ShowComponent,
    AddEditComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    NgxMatIntlTelInputModule,
    MatButtonModule,
    MatChipsModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatOptionModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
