import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditMatComponent } from './add-edit-mat.component';

describe('AddEditMatComponent', () => {
  let component: AddEditMatComponent;
  let fixture: ComponentFixture<AddEditMatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditMatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditMatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
