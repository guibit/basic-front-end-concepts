import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCursComponent } from './add-edit-curs.component';

describe('AddEditCursComponent', () => {
  let component: AddEditCursComponent;
  let fixture: ComponentFixture<AddEditCursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditCursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditCursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
