import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowCursComponent } from './show-curs.component';

describe('ShowCursComponent', () => {
  let component: ShowCursComponent;
  let fixture: ComponentFixture<ShowCursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowCursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowCursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
